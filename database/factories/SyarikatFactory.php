<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SyarikatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nama_syarikat' => $this->faker->company(),
            'alamat' => $this->faker->address(),
            'no_tel' => $this->faker->phoneNumber()
        ];
    }
}
