<?php

namespace App\Http\Controllers;

use App\Models\Syarikat;
use Illuminate\Http\Request;

class SyarikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $syarikats = Syarikat::all();
        return view('syarikat.index', ['syarikats' => $syarikats]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('syarikat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $syarikat = $this->validate(request(), [
            'nama_syarikat' => 'required',
            'alamat' => 'required',
            'no_tel' => 'required|numeric'
        ]);

        Syarikat::create($syarikat);
        return redirect('/syarikat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $syarikat = Syarikat::find($id);
        return view('syarikat.show', ['syarikat' => $syarikat]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $syarikat = Syarikat::find($id);
        return view('syarikat.edit', ['syarikat' => $syarikat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $syarikat = Syarikat::find($id);
        $syarikat->update($request->only('nama_syarikat', 'alamat', 'no_tel'));
        return redirect('/syarikat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $syarikat = Syarikat::find($id);
        $syarikat->delete();
        return redirect('/syarikat');
    }
}
