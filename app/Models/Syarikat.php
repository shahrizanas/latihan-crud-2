<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Syarikat extends Model
{
    use HasFactory;

    protected $table = 'syarikats';

    protected $fillable = ['nama_syarikat', 'alamat', 'no_tel'];
}
