@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="mt-5">Maklumat Syarikat</h2>
        <p>Berikut adalah maklumat terperinci syarikat</p>
        <form action="{{ route('syarikat.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="nama_syarikat">Nama Syarikat:</label>
                <input type="text" class="form-control" id="nama_syarikat" name="nama_syarikat"
                    placeholder="contoh: ABC Sdn Bhd">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat:</label>
                <input type="text" class="form-control" id="alamat" name="alamat"
                    placeholder="No 90, Jalan Mak Lagam">
            </div>
            <div class="form-group mb-5">
                <label for="no_tel">No Tel:</label>
                <input type="number" class="form-control" id="no_tel" name="no_tel" placeholder="contoh: 098689292">
            </div>
            <button type="button" onclick="history.back()" class="btn btn-primary">Back</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection
