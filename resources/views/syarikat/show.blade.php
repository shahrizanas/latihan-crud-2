@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="mt-5">Maklumat Syarikat</h2>
        <p>Berikut adalah maklumat terperinci syarikat</p>
        <form action="/action_page.php">
            <div class="form-group">
                <label for="usr">Nama Syarikat:</label>
                <input type="text" class="form-control" id="usr" name="username"
                    value="{{ $syarikat->nama_syarikat }}">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat:</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $syarikat->alamat }}">
            </div>
            <div class="form-group mb-5">
                <label for="no_tel">No Tel:</label>
                <input type="text" class="form-control" id="no_tel" name="no_tel" value="{{ $syarikat->no_tel }}">
            </div>
            <button type="button" onclick="history.back()" class="btn btn-primary">Back</button>
        </form>
    </div>
@endsection
