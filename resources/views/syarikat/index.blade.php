@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="">Senarai Syarikat</h2>
        <p>Ini adalah senarai ringkas syarikat</p>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Syarikat</th>
                    <th>Alamat</th>
                    <th>No Tel</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($syarikats as $num => $syarikat)
                    <tr>
                        <td>{{ $num + 1 }}</td>
                        <td>{{ $syarikat->nama_syarikat }}</td>
                        <td>{{ $syarikat->alamat }}</td>
                        <td>{{ $syarikat->no_tel }}</td>
                        <td>
                            <a href="{{ route('syarikat.show', $syarikat->id) }}" class="btn btn-success btn-sm">View</a>
                            <a href="{{ route('syarikat.edit', $syarikat->id) }}" class="btn btn-primary btn-sm">Edit</a>
                            <form action="{{ route('syarikat.destroy', $syarikat->id) }}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="delete">
                                <button class="btn btn-danger btn-sm" type="submit"
                                    onclick="return confirm('Confirm to DELETE?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
