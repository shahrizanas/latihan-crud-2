@extends('layouts.app')
@section('content')
    <div class="container">
        <h2 class="mt-5">Kemaskini Maklumat Syarikat</h2>
        <p>Kemaskini maklumat terperinci syarikat</p>
        <form action="{{ route('syarikat.update', $syarikat->id) }}" method="post">
            @method('put')
            @csrf
            <div class="form-group">
                <label for="nama_syarikat">Nama Syarikat:</label>
                <input type="text" class="form-control" id="nama_syarikat" name="nama_syarikat"
                    value="{{ $syarikat->nama_syarikat }}">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat:</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $syarikat->alamat }}">
            </div>
            <div class="form-group mb-5">
                <label for="no_tel">No Tel:</label>
                <input type="number" class="form-control" id="no_tel" name="no_tel" value="{{ $syarikat->no_tel }}">
            </div>
            <button type="button" onclick="history.back()" class="btn btn-primary">Back</button>
            <button type="submit" class="btn btn-success">Kemaskini</button>

        </form>
    </div>
@endsection
